Techstack used: WAMP

To download Wampserver, use the following link: https://download.cnet.com/WampServer-64-Bit/3000-10248_4-75544590.html

Additional downloads-jdk netbeans package: https://www.oracle.com/technetwork/java/javase/downloads/jdk-netbeans-jsp-3413139-esa.html
(uninstall any other version(s) of jdk if previously installed)

After the installation of wampserver with the default configuration is complete:

Run Wampserver.

Open the default browser(chosen during the setup).

Type in "localhost".

Select "phpMyAdmin" under the "tools" header.

Log in with username "root" and password "".

Create a new database named "password".

Within the database, create a table "users" with the following columns: name text, 
                                                                        email varchar(30), 
                                                                        password varchar(255), 
                                                                        contact int(10), 
                                                                        address varchar(255), 
                                                                        id int(11) PRIMARY KEY AUTO_INCREMENT


